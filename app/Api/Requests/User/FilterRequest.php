<?php

namespace App\Api\Requests\User;
use App\Api\Requests\Request;

class FilterRequest extends Request
{
    public function rules()
    {
        return [
            'hobby_id' => 'int|exists:hobbies,id',
        ];
    }
}
